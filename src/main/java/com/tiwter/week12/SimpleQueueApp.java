package com.tiwter.week12;

import java.util.LinkedList;
import java.util.Scanner;

public class SimpleQueueApp {
    private static Scanner sc = new Scanner(System.in);
    private static int manu = 0;
    private static LinkedList<String> queue = new LinkedList();
    private static String current;
    public static void main(String[] args) {
        while(true) {
            shoeQueue();
            printManu();
            inputManu();
        }     
    }

    private static void shoeQueue() {
        System.out.println(queue);
    }

    private static void inputManu() {
        try {
            System.out.print("Please input manu (1-3): ");
            manu = sc.nextInt();
            switch(manu) {
            case 1:
                newQueue();
                break;
            case 2:
                getQueue();
                break;
            case 3:
                exit();
                break;
            default:
                System.out.print("Please input 1-3: ");
            }
        }   catch(Exception e) {
            System.out.println("Error: Please input 1-3");
            sc.next();
        }
    }

    private static void getQueue() {
        if(queue.isEmpty()) {
            System.out.println("Queue Empty");
            return;
        }
        current = queue.remove();
        System.out.println("Current: " + current);
    }

    private static void exit() {
        System.out.println("Byb Byb!!!");
        System.exit(0);
    }

    private static void newQueue() {
        System.out.print("Please input name: ");
        String name = sc.next();
        queue.add(name);
    }

    private static void printManu() {
        System.out.println("----Manu----");
        System.out.println("1. New Queue");
        System.out.println("2. Get Queue");
        System.out.println("3. Exit");
        System.out.println("-------------");
    }
}
